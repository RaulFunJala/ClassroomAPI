﻿using FluentValidation;
using Models;

namespace BussinessLogic.Validators;

public class ImageValidator : AbstractValidator<Image>
{
    public ImageValidator()
    {
        this.RuleFor(img => img.File.Files)
        .NotNull()
        .Must(x => x!.Length > 0);

        this.RuleFor(img => img)
        .Must(x => x.File.Files!.Length <= x.FileSizeLimit)
        .WithMessage("File size supported is up to 500KB");

        this.RuleFor(img => img.Extension)
        .NotNull()
        .NotEmpty()
        .Must(ext => Image.PermittedExtensions.Contains(ext));
    }
}