﻿using FluentValidation;
using Models;

namespace BussinessLogic.Validators
{
    internal class StudentValidator : AbstractValidator<Student>
    {
        public StudentValidator()
        {
            this.RuleFor(student => student.FirstName)
                .NotNull()
                .NotEmpty()
                .MaximumLength(Student.MaxFirstNameLength)
                .WithMessage("The first name have to be less than 25 characters");

            this.RuleFor(student => student.LastName)
                .NotNull()
                .NotEmpty()
                .MaximumLength(Student.MaxLastNameLength)
                .WithMessage("The last name have to be less than 25 characters");

            this.RuleFor(student => student.Email)
                .NotNull()
                .NotEmpty()
                .EmailAddress()
                .MaximumLength(Student.MaxEmailLength);

            this.RuleFor(student => student.BirthDay)
                .NotNull();

            this.RuleFor(student => student.Image)
                .NotNull()
                .NotEmpty()
                .MaximumLength(Student.MaxImageLength)
                .WithMessage("The link of the image have to be less than 125 characters");
        }
    }
}