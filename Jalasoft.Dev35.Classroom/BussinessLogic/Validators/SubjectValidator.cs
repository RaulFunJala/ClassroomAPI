using FluentValidation;
using Models;

namespace BussinessLogic.Validators;

public class SubjectValidator : AbstractValidator<Subject>
{
    public SubjectValidator()
    {
        this.RuleFor(subject => subject.SubjectName)
        .NotNull()
        .NotEmpty()
        .MaximumLength(Subject.MaxNameLength);

        this.RuleFor(subject => subject.StartingDate)
        .NotNull();

        this.RuleFor(subject => subject.TrainerName)
        .NotNull()
        .NotEmpty()
        .MaximumLength(Subject.MaxTrainerLength);

        this.RuleFor(subject => subject.StartTime)
        .NotNull()
        .NotEmpty()
        .MaximumLength(Subject.MaxTimeLenght);

        this.RuleFor(subject => subject.EndTime)
        .NotNull()
        .NotEmpty()
        .MaximumLength(Subject.MaxTrainerLength)
        .GreaterThan(subject => subject.StartTime);

        this.RuleFor(subject => (int)subject.DaysOfWeek)
        .GreaterThan(0)
        .LessThanOrEqualTo(31);

        this.RuleFor(subject => subject.Image)
        .NotNull()
        .NotEmpty()
        .MaximumLength(Subject.MaxImageLength);

        this.RuleFor(subject => subject.CourseId)
        .NotNull();
    }
}