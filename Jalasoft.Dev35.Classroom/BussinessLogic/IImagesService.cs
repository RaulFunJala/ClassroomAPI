using Models;

namespace BussinessLogic;

public interface IImagesService
{
    public string SaveImage(Image image);
    public byte[]? GetImage(string fileName);
}