using Models;

namespace BussinessLogic;

public interface ISubjectService
{
    public List<Subject> GetByCourse(int courseId);
    public Subject? Get(int id);
    public Subject? Create(Subject newSubject);
}