﻿using BussinessLogic;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.Util;

namespace Classroom.Controllers;

[ApiController]
[Route("api/[controller]")]
public class StudentController : ControllerBase
{
    private readonly IStudentService studentService;
    public StudentController(IStudentService studentService)
    {
        this.studentService = studentService;
    }

    [HttpGet]
    [ProducesResponseType(typeof(IEnumerable<Student>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult GetAll([FromQuery]bool onlyNotRegistered)
    {
        try
        {
            IEnumerable<Student> students = this.studentService.GetAll(onlyNotRegistered);
            if (!students.Any())
            {
                return this.NoContent();
            }

            return this.Ok(students);
        }
        catch (InvalidInputException ex)
        {
            return this.StatusCode(400, ex.Message);
        }
        catch (Exception ex)
        {
            return this.StatusCode(500, ex.Message);
        }
    }

    [HttpGet("ByCourseId")]
    [ProducesResponseType(typeof(IEnumerable<Student>), StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status204NoContent)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult GetByCourseId(int courseId)
    {
        try
        {
            IEnumerable<Student>? students = this.studentService.GetByCourseId(courseId);
            if (students is null)
            {
                return this.NotFound();
            }

            if (!students.Any())
            {
                return this.NoContent();
            }

            return this.Ok(students);
        }
        catch (InvalidInputException ex)
        {
            return this.StatusCode(400, ex.Message);
        }
        catch (Exception ex)
        {
            return this.StatusCode(500, ex.Message);
        }
    }

    [HttpPost]
    [ProducesResponseType(typeof(IEnumerable<Student>), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status400BadRequest)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Post([FromBody] Student newStudent)
    {
        try
        {
            Student? student = this.studentService.Create(newStudent);
            return this.Created("Student", student);
        }
        catch (InvalidInputException ex)
        {
            return this.BadRequest(ex.Message);
        }
        catch (Exception ex)
        {
            return this.StatusCode(500, ex.Message);
        }
    }

    [HttpPut("{id}")]
    [ProducesResponseType(typeof(IEnumerable<Student>), StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public IActionResult Put(int id, int? courseId)
    {
        try
        {
            Student? student = this.studentService.Update(id, courseId);
            if (student is null)
            {
                return this.NotFound();
            }

            return this.Created("Student", student);
        }
        catch (InvalidInputException ex)
        {
            return this.StatusCode(400, ex.Message);
        }
        catch (Exception ex)
        {
            return this.StatusCode(500, ex.Message);
        }
    }
}