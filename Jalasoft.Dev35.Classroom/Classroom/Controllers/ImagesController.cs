﻿using BussinessLogic;
using Microsoft.AspNetCore.Mvc;
using Models;
using Models.Util;

namespace Classroom.Controllers;
[Route("api/[controller]")]
public class ImagesController : Controller
{
    private readonly IWebHostEnvironment webHostEnvironment;
    private readonly IImagesService service;
    private readonly long fileSizeLimit;

    public ImagesController(IWebHostEnvironment webHostEnvironment, IConfiguration config, IImagesService service)
    {
        this.webHostEnvironment = webHostEnvironment;
        this.service = service;
        this.fileSizeLimit = config.GetValue<long>("FileSizeLimit");
    }

    // GET: api/Images/student/image.png
    [HttpGet("{folder}/{imageName}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public IActionResult Get(string folder, string imageName)
    {
        try
        {
            string path = $"{this.webHostEnvironment.WebRootPath}/uploads/images/{folder}/";
            string filePath = path + imageName;
            byte[]? image = this.service.GetImage(filePath);
            if (image is not null)
            {
                string ext = Path.GetExtension(filePath).ToLowerInvariant();
                return this.File(image, $"image/{ext[1..]}");
            }

            return this.NotFound();
        }
        catch (Exception ex)
        {
            return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }

    // POST api/Images/upload?folder=course&fileName=Dev35
    [HttpPost("upload")]
    [ProducesResponseType(typeof(string), StatusCodes.Status201Created)]
    [ProducesResponseType(typeof(string), StatusCodes.Status400BadRequest)]
    [ProducesResponseType(typeof(string), StatusCodes.Status500InternalServerError)]
    public IActionResult UploadImage([FromQuery] string folder, [FromQuery] string fileName, [FromForm] FileUpload imageFile)
    {
        try
        {
            string baseUrl = $"{this.HttpContext.Request.Host}/api/Images";
            string basePath = $"{this.webHostEnvironment.WebRootPath}";
            var image = new Image(imageFile, baseUrl, basePath, folder, fileName, this.fileSizeLimit);
            string imageUrl = this.service.SaveImage(image);
            return this.Ok(imageUrl);
        }
        catch (InvalidInputException ex)
        {
            return this.BadRequest(ex.Message);
        }
        catch (Exception ex)
        {
            return this.StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
        }
    }
}