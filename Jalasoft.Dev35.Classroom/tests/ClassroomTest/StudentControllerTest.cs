﻿using System;
using System.Collections.Generic;
using BussinessLogic;
using Classroom.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Models;
using Models.Util;
using Moq;
using Xunit;

namespace ClassroomTest
{
    public class StudentControllerTest
    {
        private readonly StudentController controller;
        private readonly Mock<IStudentService> studentServiceMock;

        public StudentControllerTest()
        {
            this.studentServiceMock = new Mock<IStudentService>();
            this.controller = new StudentController(this.studentServiceMock.Object);
        }

        [Fact]
        public void GetAll_Returns_200_Ok()
        {
            this.studentServiceMock.Setup(moq => moq.GetAll(true)).Returns(new List<Student> { new Student(), new Student() });
            Assert.IsType<OkObjectResult>(this.controller.GetAll(true));
        }

        [Fact]
        public void GetAll_Returns_204_NoContent()
        {
            this.studentServiceMock.Setup(moq => moq.GetAll(true)).Returns(new List<Student>());
            IActionResult result = this.controller.GetAll(true);
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(204, statusCodeResult.StatusCode);
        }

        [Fact]
        public void GetAll_Returns_500_InternalError()
        {
            this.studentServiceMock.Setup(moq => moq.GetAll(true)).Throws<Exception>();
            IActionResult result = this.controller.GetAll(true);
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(500, statusCodeResult.StatusCode);
        }

        [Fact]
        public void GetByCourseId_Returns_200_Ok()
        {
            this.studentServiceMock.Setup(moq => moq.GetByCourseId(It.IsAny<int>())).Returns(new List<Student> { new Student(), new Student() });
            Assert.IsType<OkObjectResult>(this.controller.GetByCourseId(1));
        }

        [Fact]
        public void GetByCourseId_Returns_204_NoContent()
        {
            this.studentServiceMock.Setup(moq => moq.GetByCourseId(It.IsAny<int>())).Returns(new List<Student>());
            IActionResult result = this.controller.GetByCourseId(It.IsAny<int>());
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(204, statusCodeResult.StatusCode);
        }

        [Fact]
        public void GetByCourseId_Returns_500_InternalError()
        {
            this.studentServiceMock.Setup(moq => moq.GetByCourseId(It.IsAny<int>())).Throws<Exception>();
            IActionResult result = this.controller.GetByCourseId(It.IsAny<int>());
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(500, statusCodeResult.StatusCode);
        }

        [Fact]
        public void Post_Returns_201_Created()
        {
            var newStudent = new Student();
            newStudent.FirstName = "FirstName";
            newStudent.LastName = "LastName";
            newStudent.Email = "abc@funjala.com";
            newStudent.BirthDay = new DateTime(2022, 10, 1);
            newStudent.Image = "https://localhost:7050/image.png";
            newStudent.CountryId = 1;
            this.studentServiceMock.Setup(moq => moq.Create(It.IsAny<Student>())).Returns(new Student());
            IActionResult result = this.controller.Post(newStudent);
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(201, statusCodeResult.StatusCode);
        }

        [Fact]
        public void Post_Returns_400_BadRequest()
        {
            var newStudent = new Student();
            this.studentServiceMock.Setup(moq => moq.Create(It.IsAny<Student>())).
                Throws<InvalidInputException>();
            Assert.IsType<BadRequestObjectResult>(this.controller.Post(newStudent));
        }

        [Fact]
        public void Post_Returns_500_InternalError()
        {
            this.studentServiceMock.Setup(moq => moq.Create(It.IsAny<Student>())).Throws<Exception>();
            IActionResult result = this.controller.Post(new Student());
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(500, statusCodeResult.StatusCode);
        }

        [Fact]
        public void Put_Returns_201_Created()
        {
            this.studentServiceMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<int>())).Returns(new Student());
            IActionResult result = this.controller.Put(1, 1);
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(201, statusCodeResult.StatusCode);
        }

        [Fact]
        public void Put_Returns_404_NotFound()
        {
            this.studentServiceMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<int>())).Returns<Student>(null);
            IActionResult result = this.controller.Put(1, 1);
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(404, statusCodeResult.StatusCode);
        }

        [Fact]
        public void Put_Returns_400_InvalidInputException()
        {
            this.studentServiceMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<int>())).Throws<InvalidInputException>();
            IActionResult result = this.controller.Put(1, 1);
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(400, statusCodeResult.StatusCode);
        }

        [Fact]
        public void Put_Returns_500_InternalError()
        {
            this.studentServiceMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<int>())).Throws<Exception>();
            IActionResult result = this.controller.Put(1, 1);
            var statusCodeResult = (IStatusCodeActionResult)result;
            Assert.Equal(500, statusCodeResult.StatusCode);
        }
    }
}
