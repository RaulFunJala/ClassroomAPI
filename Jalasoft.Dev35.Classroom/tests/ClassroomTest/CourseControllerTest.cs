﻿namespace ClassroomTest;

using System;
using BussinessLogic;
using Classroom.Controllers;
using Models;
using Moq;
using System.Collections.Generic;
using Xunit;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Infrastructure;
using Models.Util;

public class CourseControllerTest
{
    private CourseController controller;
    private Mock<ICourseService> courseServiceMock;

    public CourseControllerTest()
    {
        this.courseServiceMock = new Mock<ICourseService>();
        this.controller = new CourseController(this.courseServiceMock.Object);
    }

    [Fact]
    public void GetAll_Returns_200_Ok()
    {
        this.courseServiceMock.Setup(moq => moq.GetAll()).Returns(new List<Course> { new Course(), new Course() });
        Assert.IsType<OkObjectResult>(this.controller.GetAll());
    }

    [Fact]
    public void GetAll_Returns_204_NoContent()
    {
        this.courseServiceMock.Setup(moq => moq.GetAll()).Returns<List<Course>>(null);
        IActionResult result = this.controller.GetAll();
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(204, statusCodeResult.StatusCode);
    }

    [Fact]
    public void GetAll_Returns_500_InternalError()
    {
        this.courseServiceMock.Setup(moq => moq.GetAll()).Throws<Exception>();
        IActionResult result = this.controller.GetAll();
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(500, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Get_Returns_200_Ok()
    {
        this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
        IActionResult result = this.controller.Get(2);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(200, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Get_Returns_404_NotFound()
    {
        this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns<Course>(null);
        IActionResult result = this.controller.Get(2);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(404, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Get_Returns_400_InvalidOperationException()
    {
        this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Throws<InvalidOperationException>();
        IActionResult result = this.controller.Get(2);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(400, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Get_Returns_500_InternalError()
    {
        this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Throws<Exception>();
        IActionResult result = this.controller.Get(2);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(500, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Post_Returns_201_Created()
    {
        var newCourse = new Course();
        newCourse.CourseName = "Dev-32";
        newCourse.Description = "This is a desc";
        newCourse.StartingDate = new DateTime(2022, 1, 1);
        newCourse.EndingDate = new DateTime(2022, 10, 1);
        newCourse.Image = "https://localhost:7050/image.png";
        this.courseServiceMock.Setup(moq => moq.Create(It.IsAny<Course>())).Returns(new Course());
        IActionResult result = this.controller.Post(newCourse);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(201, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Post_Returns_400_BadRequest()
    {
        var newCourse = new Course();
        newCourse.CourseName = "Dev-32";
        newCourse.Description = "This is a desc";
        newCourse.StartingDate = new DateTime(2022, 1, 1);
        newCourse.EndingDate = new DateTime(2022, 10, 1);
        newCourse.Image = "https://localhost:7050/image.png";
        this.courseServiceMock.Setup(moq => moq.Create(It.IsAny<Course>())).Returns<Course>(null);
        IActionResult result = this.controller.Post(newCourse);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(400, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Post_Returns_400_InvalidInputException()
    {
        this.courseServiceMock.Setup(moq => moq.Create(It.IsAny<Course>())).Throws<InvalidInputException>();
        IActionResult result = this.controller.Post(new Course());
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(400, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Post_Returns_500_InternalError()
    {
        this.courseServiceMock.Setup(moq => moq.Create(It.IsAny<Course>())).Throws<Exception>();
        IActionResult result = this.controller.Post(new Course());
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(500, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Put_Returns_200_Success()
    {
        var newCourse = new Course();
        newCourse.CourseName = "Dev-32";
        newCourse.Description = "This is a desc";
        newCourse.StartingDate = new DateTime(2022, 1, 1);
        newCourse.EndingDate = new DateTime(2022, 10, 1);
        newCourse.Image = "https://localhost:7050/image.png";
        this.courseServiceMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<Course>())).Returns(new Course());
        IActionResult result = this.controller.Put(1, new Course());
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(200, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Put_Returns_404_NotFound()
    {
        var newCourse = new Course();
        newCourse.CourseName = "Dev-32";
        newCourse.Description = "This is a desc";
        newCourse.StartingDate = new DateTime(2022, 1, 1);
        newCourse.EndingDate = new DateTime(2022, 10, 1);
        newCourse.Image = "https://localhost:7050/image.png";
        this.courseServiceMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<Course>())).Returns<Course>(null);
        IActionResult result = this.controller.Put(1, new Course());
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(404, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Put_Returns_400_InvalidInputException()
    {
        this.courseServiceMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<Course>())).Throws<InvalidInputException>();
        IActionResult result = this.controller.Put(1, new Course());
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(400, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Put_Returns_500_InternalError()
    {
        this.courseServiceMock.Setup(moq => moq.Update(It.IsAny<int>(), It.IsAny<Course>())).Throws<Exception>();
        IActionResult result = this.controller.Put(1, new Course());
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(500, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Delete_Returns_200_NoContent()
    {
        this.courseServiceMock.Setup(moq => moq.Delete(It.IsAny<int>())).Returns(true);
        IActionResult result = this.controller.Delete(1);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(204, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Delete_Returns_404_NotFound()
    {
        this.courseServiceMock.Setup(moq => moq.Delete(It.IsAny<int>())).Returns<bool>(null);
        IActionResult result = this.controller.Delete(1);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(404, statusCodeResult.StatusCode);
    }

    [Fact]
    public void Delete_Returns_500_InternalError()
    {
        this.courseServiceMock.Setup(moq => moq.Delete(It.IsAny<int>())).Throws<Exception>();
        IActionResult result = this.controller.Delete(2);
        var statusCodeResult = (IStatusCodeActionResult)result;
        Assert.Equal(500, statusCodeResult.StatusCode);
    }
}
