﻿using System;
using System.Collections.Generic;
using BussinessLogic;
using Classroom.Controllers;
using Microsoft.AspNetCore.Mvc;
using Models;
using Moq;
using Xunit;

namespace ClassroomTest;

public class CountryControllerTest
{
    private readonly Mock<ICountryService> serviceMock;
    private readonly CountryController controller;

    public CountryControllerTest()
    {
        this.serviceMock = new Mock<ICountryService>();
        this.controller = new CountryController(this.serviceMock.Object);
    }

    [Fact]
    public void GetAll_Returns_200Ok()
    {
        this.serviceMock.Setup(x => x.GetAll()).Returns(new List<Country> { new Country() });
        Assert.IsType<OkObjectResult>(this.controller.GetAll());
    }

    [Fact]
    public void GetAll_Returns_204NoContent()
    {
        this.serviceMock.Setup(x => x.GetAll()).Returns(new List<Country>());
        Assert.IsType<NoContentResult>(this.controller.GetAll());
    }

    [Fact]
    public void GetAll_Returns_500InternalError()
    {
        this.serviceMock.Setup(x => x.GetAll()).Throws<Exception>();
        Assert.IsType<ObjectResult>(this.controller.GetAll());
    }
}