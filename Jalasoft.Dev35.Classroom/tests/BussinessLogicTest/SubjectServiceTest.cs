using System;
using System.Collections.Generic;
using BussinessLogic;
using DataAccess;
using Models;
using Models.Util;
using Models.Util.Enums;
using Moq;
using Xunit;

namespace BussinessLogicTest;
public class SubjectServiceTest
{
    private readonly SubjectService service;
    private readonly Mock<ISubjectRepository> subjectRepositoryMock;
    private readonly Mock<ICourseService> courseServiceMock;

    public SubjectServiceTest()
    {
        this.subjectRepositoryMock = new Mock<ISubjectRepository>();
        this.courseServiceMock = new Mock<ICourseService>();
        this.service = new SubjectService(this.subjectRepositoryMock.Object, this.courseServiceMock.Object);
    }

    [Fact]
    public void GetByCourse_ReturnsListOfSubjects()
    {
        this.subjectRepositoryMock.Setup(moq => moq.GetByCourse(It.IsAny<int>())).Returns(new List<Subject>());
        List<Subject> subjects = this.service.GetByCourse(1);
        Assert.NotNull(subjects);
    }

    [Fact]
    public void GetByCourse_ThrowsException()
    {
        this.subjectRepositoryMock.Setup(moq => moq.GetByCourse(It.IsAny<int>())).Throws<Exception>();
        Assert.Throws<Exception>(() => this.service.GetByCourse(1));
    }

    [Fact]
    public void Get_ReturnsSubject()
    {
        this.subjectRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Subject());
        Subject? subject = this.service.Get(1);
        Assert.NotNull(subject);
        Assert.IsType<Subject>(subject);
    }

    [Fact]
    public void Get_ReturnsNullSubject()
    {
        this.subjectRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns<Subject?>(null);
        Subject? subject = this.service.Get(1);
        Assert.Null(subject);
    }

    [Fact]
    public void Get_ThrowsException()
    {
        this.subjectRepositoryMock.Setup(moq => moq.Get(It.IsAny<int>())).Throws<Exception>();
        Assert.Throws<Exception>(() => this.service.Get(1));
    }

    [Fact]
    public void Create_ReturnsSubject()
    {
        Subject newSubject = new Subject();
        newSubject.SubjectName = "Backend";
        newSubject.StartingDate = DateTime.Today.AddMonths(1);
        newSubject.TrainerName = "Josue Lima";
        newSubject.StartTime = "16:30";
        newSubject.EndTime = "18:00";
        newSubject.DaysOfWeek = Day.Tuesday | Day.Thursday;
        newSubject.Image = "https://localhost:7050/api/Images/subject/1-Docker.png";
        newSubject.CourseId = 1;
        this.subjectRepositoryMock.Setup(moq => moq.Create(It.IsAny<Subject>())).Returns(new Subject());
        this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
        this.subjectRepositoryMock.Setup(moq => moq.GetByCourse(It.IsAny<int>())).Returns(new List<Subject>());
        Subject? subject = this.service.Create(newSubject);
        Assert.NotNull(subject);
        Assert.IsType<Subject>(subject);
    }

    [Fact]
    public void Create_ThrowsInvalidInputException_OnSubjectValidation()
    {
        Subject newSubject = new Subject();
        newSubject.StartingDate = DateTime.Today.AddMonths(1);
        newSubject.TrainerName = "Josue Lima";
        newSubject.StartTime = "16:30";
        newSubject.EndTime = "18:00";
        newSubject.DaysOfWeek = Day.Tuesday | Day.Thursday;
        newSubject.Image = "https://localhost:7050/api/Images/subject/1-Docker.png";
        newSubject.CourseId = 1;
        Assert.Throws<InvalidInputException>(() => this.service.Create(newSubject));
    }

    [Fact]
    public void Create_ThrowsInvalidInputException_OnInvalidCourseId()
    {
        Subject newSubject = new Subject();
        newSubject.SubjectName = "Backend";
        newSubject.StartingDate = DateTime.Today.AddMonths(1);
        newSubject.TrainerName = "Josue Lima";
        newSubject.StartTime = "16:30";
        newSubject.EndTime = "18:00";
        newSubject.DaysOfWeek = Day.Tuesday | Day.Thursday;
        newSubject.Image = "https://localhost:7050/api/Images/subject/1-Docker.png";
        newSubject.CourseId = 1;
        this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns<Course?>(null);
        Assert.Throws<InvalidInputException>(() => this.service.Create(newSubject));
    }

    [Fact]
    public void Create_ThrowsInvalidInputException_OnSubjectNameRepeated()
    {
        Subject newSubject = new Subject();
        newSubject.SubjectName = "Backend";
        newSubject.StartingDate = DateTime.Today.AddMonths(1);
        newSubject.TrainerName = "Josue Lima";
        newSubject.StartTime = "16:30";
        newSubject.EndTime = "18:00";
        newSubject.DaysOfWeek = Day.Tuesday | Day.Thursday;
        newSubject.Image = "https://localhost:7050/api/Images/subject/1-Docker.png";
        newSubject.CourseId = 1;
        Subject oldSubject = new Subject();
        oldSubject.SubjectName = "Backend";
        List<Subject> oldSubjects = new List<Subject> { oldSubject };
        this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
        this.subjectRepositoryMock.Setup(moq => moq.GetByCourse(It.IsAny<int>())).Returns(oldSubjects);
        Assert.Throws<InvalidInputException>(() => this.service.Create(newSubject));
    }

    [Fact]
    public void Create_ThrowsException()
    {
        Subject newSubject = new Subject();
        newSubject.SubjectName = "Backend";
        newSubject.StartingDate = DateTime.Today.AddMonths(1);
        newSubject.TrainerName = "Josue Lima";
        newSubject.StartTime = "16:30";
        newSubject.EndTime = "18:00";
        newSubject.DaysOfWeek = Day.Tuesday | Day.Thursday;
        newSubject.Image = "https://localhost:7050/api/Images/subject/1-Docker.png";
        newSubject.CourseId = 1;
        this.subjectRepositoryMock.Setup(moq => moq.Create(It.IsAny<Subject>())).Throws<Exception>();
        this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
        this.subjectRepositoryMock.Setup(moq => moq.GetByCourse(It.IsAny<int>())).Returns(new List<Subject>());
        Assert.Throws<Exception>(() => this.service.Create(newSubject));
    }
}