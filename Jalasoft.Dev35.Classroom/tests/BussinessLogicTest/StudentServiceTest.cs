﻿using System;
using System.Collections.Generic;
using System.Linq;
using BussinessLogic;
using DataAccess;
using Models;
using Moq;
using Xunit;

namespace BussinessLogicTest
{
    public class StudentServiceTest
    {
        private readonly StudentService service;
        private readonly Mock<IStudentRepository> studentRepositoryMock;
        private readonly Mock<ICourseService> courseServiceMock;

        public StudentServiceTest()
        {
            this.studentRepositoryMock = new Mock<IStudentRepository>();
            this.courseServiceMock = new Mock<ICourseService>();
            this.service = new StudentService(this.studentRepositoryMock.Object, this.courseServiceMock.Object);
        }

        [Fact]
        public void GetAll_ReturnsListOfStudents()
        {
            this.studentRepositoryMock.Setup(moq => moq.GetAll()).Returns(new List<Student>());
            List<Student> students = this.service.GetAll(true).ToList();
            Assert.NotNull(students);
            Assert.IsType<List<Student>>(students);
        }

        [Fact]
        public void GetAll_ReturnsListOfStudentsOnlyNotRegistered()
        {
            this.studentRepositoryMock.Setup(moq => moq.GetAll()).Returns(new List<Student>());
            List<Student> students = this.service.GetAll(false).ToList();
            Assert.NotNull(students);
            Assert.IsType<List<Student>>(students);
        }

        [Fact]
        public void GetAll_ThrowsException()
        {
            this.studentRepositoryMock.Setup(moq => moq.GetAll()).Throws<Exception>();
            Assert.Throws<Exception>(() => this.service.GetAll(true));
        }

        [Fact]
        public void GetByCourse_ReturnsListOfStudent()
        {
            this.courseServiceMock.Setup(moq => moq.Get(It.IsAny<int>())).Returns(new Course());
            this.studentRepositoryMock.Setup(moq => moq.GetAll()).Returns(new List<Student>());
            List<Student>? students = this.service.GetByCourseId(1) !.ToList();
            Assert.NotNull(students);
            Assert.IsType<List<Student>>(students);
        }
    }
}
