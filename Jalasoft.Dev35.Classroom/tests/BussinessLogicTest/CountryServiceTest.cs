﻿using System;
using System.Collections.Generic;
using BussinessLogic;
using DataAccess;
using Models;
using Moq;
using Xunit;
namespace BussinessLogicTest;

public class CountryServiceTest
{
    private readonly Mock<ICountryRepository> repositoryMock;
    private readonly CountryService service;
    public CountryServiceTest()
    {
        this.repositoryMock = new Mock<ICountryRepository>();
        this.service = new CountryService(this.repositoryMock.Object);
    }

    [Fact]
    public void GetAll_Returns_Countries()
    {
        this.repositoryMock.Setup(x => x.GetAll()).Returns(new List<Country>());
        Assert.IsType<List<Country>>(this.service.GetAll());
    }

    [Fact]
    public void GetAll_Throws_Exception()
    {
        this.repositoryMock.Setup(x => x.GetAll()).Throws<Exception>();
        Assert.Throws<Exception>(() => this.service.GetAll());
    }
}