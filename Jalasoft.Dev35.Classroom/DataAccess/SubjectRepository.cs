using System.Data;
using System.Data.SqlClient;
using Dapper;
using Models;

namespace DataAccess;

public class SubjectRepository : ISubjectRepository
{
    private readonly string connString;

    public SubjectRepository(SQLConnection sqlConnection)
    {
        this.connString = sqlConnection.ConnectionString;
    }

    public List<Subject> GetByCourse(int courseId)
    {
        using IDbConnection db = new SqlConnection(this.connString);
        var procedure = "spSubject_GetAll";
        var parameters = new DynamicParameters();
        parameters.Add("@courseId", courseId, DbType.Int32, ParameterDirection.Input, int.MaxValue);
        List<Subject> subjects = db.Query<Subject>(procedure, parameters, commandType: CommandType.StoredProcedure).ToList();
        return subjects;
    }

    public Subject? Get(int id)
    {
        using IDbConnection db = new SqlConnection(this.connString);
        var procedure = "spSubject_Get";
        var parameters = new DynamicParameters();
        parameters.Add("@id", id, DbType.Int32, ParameterDirection.Input, int.MaxValue);
        Subject subject = db.QuerySingleOrDefault<Subject>(procedure, parameters, commandType: CommandType.StoredProcedure);
        return subject;
    }

    public Subject? Create(Subject newSubject)
    {
        using IDbConnection db = new SqlConnection(this.connString);
        var procedure = "spSubject_Create";
        var parameters = new DynamicParameters();
        parameters.Add("@subjectName", newSubject.SubjectName, DbType.String, ParameterDirection.Input, Subject.MaxNameLength);
        parameters.Add("@startingDate", newSubject.StartingDate, DbType.Date, ParameterDirection.Input);
        parameters.Add("@trainerName", newSubject.TrainerName, DbType.String, ParameterDirection.Input, Subject.MaxTrainerLength);
        parameters.Add("@startTime", newSubject.StartTime, DbType.String, ParameterDirection.Input, Subject.MaxTimeLenght);
        parameters.Add("@endTime", newSubject.EndTime, DbType.String, ParameterDirection.Input, Subject.MaxTimeLenght);
        parameters.Add("@daysOfWeek", newSubject.DaysOfWeek, DbType.Int32, ParameterDirection.Input, byte.MaxValue);
        parameters.Add("@image", newSubject.Image, DbType.String, ParameterDirection.Input, Subject.MaxImageLength);
        parameters.Add("@courseId", newSubject.CourseId, DbType.Int32, ParameterDirection.Input, int.MaxValue);
        Subject subject = db.QuerySingleOrDefault<Subject>(procedure, parameters, commandType: CommandType.StoredProcedure);
        return subject;
    }
}