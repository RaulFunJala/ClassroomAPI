using Models;

namespace DataAccess;

public interface ISubjectRepository
{
    public List<Subject> GetByCourse(int courseId);
    public Subject? Get(int id);
    public Subject? Create(Subject newSubject);
}