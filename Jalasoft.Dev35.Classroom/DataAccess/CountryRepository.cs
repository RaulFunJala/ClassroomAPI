﻿using System.Data;
using System.Data.SqlClient;
using Dapper;
using Models;
using Models.Util;

namespace DataAccess;

public class CountryRepository : ICountryRepository
{
    private readonly SQLConnection sqlConnection;
    public CountryRepository(SQLConnection sqlConnection)
    {
        this.sqlConnection = sqlConnection;
    }

    public IEnumerable<Country> GetAll()
    {
        using IDbConnection db = new SqlConnection(this.sqlConnection.ConnectionString);
        var storeProc = "spCountry_GetAll";
        return db.Query<Country>(storeProc, commandType: CommandType.StoredProcedure).ToList();
    }
}
