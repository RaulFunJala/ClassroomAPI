﻿namespace DataAccess;

using Dapper;
using Models;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Collections.Generic;

public class CourseRepository : ICourseRepository
{
    private readonly string connectionString;

    public CourseRepository(SQLConnection sqlConnection)
    {
        this.connectionString = sqlConnection.ConnectionString;
    }

    private IDbConnection Connection => new SqlConnection(this.connectionString);

    public List<Course> GetAll()
    {
        using IDbConnection db = this.Connection;
        var procedure = "spCourse_GetAll";
        return db.Query<Course>(procedure, commandType: CommandType.StoredProcedure).ToList();
    }

    public Course? Get(int id)
    {
        using IDbConnection db = this.Connection;
        var procedure = "spCourse_Get";
        var parameters = new DynamicParameters();
        parameters.Add("@Id", id, DbType.Int32, ParameterDirection.Input, int.MaxValue);
        return db.QuerySingleOrDefault<Course>(procedure, parameters, commandType: CommandType.StoredProcedure);
    }

    public Course? Save(Course course)
    {
        using IDbConnection db = this.Connection;
        var procedure = "spCourse_Create";
        var parameters = new DynamicParameters();
        parameters.Add("@CourseName", course.CourseName, DbType.String, ParameterDirection.Input, 25);
        parameters.Add("@Description", course.Description, DbType.String, ParameterDirection.Input, 125);
        parameters.Add("@StartingDate", course.StartingDate, DbType.Date, ParameterDirection.Input);
        parameters.Add("@EndingDate", course.EndingDate, DbType.Date, ParameterDirection.Input);
        parameters.Add("@Image", course.Image, DbType.String, ParameterDirection.Input, 125);
        return db.QuerySingleOrDefault<Course>(procedure, parameters, commandType: CommandType.StoredProcedure);
    }

    public Course Update(int id, Course course)
    {
        using IDbConnection db = this.Connection;
        var procedure = "spCourse_Update";
        var parameters = new DynamicParameters();
        parameters.Add("@Id", id, DbType.Int32, ParameterDirection.Input, int.MaxValue);
        parameters.Add("@CourseName", course.CourseName, DbType.String, ParameterDirection.Input, 25);
        parameters.Add("@Description", course.Description, DbType.String, ParameterDirection.Input, 125);
        parameters.Add("@StartingDate", course.StartingDate, DbType.Date, ParameterDirection.Input);
        parameters.Add("@EndingDate", course.EndingDate, DbType.Date, ParameterDirection.Input);
        parameters.Add("@Image", course.Image, DbType.String, ParameterDirection.Input, 125);
        return db.QuerySingleOrDefault<Course>(procedure, parameters, commandType: CommandType.StoredProcedure);
    }

    public bool Delete(int id)
    {
        using IDbConnection db = this.Connection;
        var procedure = "spCourse_Delete";
        var parameters = new DynamicParameters();
        parameters.Add("@Id", id, DbType.Int32, ParameterDirection.Input, int.MaxValue);
        db.Execute(procedure, parameters, commandType: CommandType.StoredProcedure);
        return true;
    }
}
