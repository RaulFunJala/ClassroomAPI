using Models;

namespace DataAccess;

public interface IImagesRepository
{
    public string SaveImage(Image image);
    public byte[] GetImage(string fileName);
}