using Models.Util;
using Models.Util.Enums;

namespace Models;

public class Subject
{
    public static readonly int MaxNameLength = 25;
    public static readonly int MaxImageLength = 125;
    public static readonly int MaxTrainerLength = 50;
    public static readonly int MaxTimeLenght = 5;

    public int? Id { get; init; } = null;

    public string? SubjectName { get; set; }

    public DateTime? StartingDate { get; set; } = null;

    public string? TrainerName { get; set; }

    public string? StartTime { get; set; }

    public string? EndTime { get; set; }

    public Day DaysOfWeek { get; set; }

    public string? Image { get; set; }

    public int? CourseId { get; set; } = null;
}