using Microsoft.AspNetCore.Http;

namespace Models;

public class FileUpload
{
    public FileUpload(IFormFile? file)
    {
        this.Files = file;
    }

    public FileUpload()
    {
    }

    public IFormFile? Files { get; set; }
}