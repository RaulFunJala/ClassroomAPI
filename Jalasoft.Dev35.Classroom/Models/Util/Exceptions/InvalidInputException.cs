﻿namespace Models.Util;

public class InvalidInputException : ClassroomException
{
    public InvalidInputException()
        : base(1, "Invalid input data")
    {
    }

    public InvalidInputException(string? message)
        : base(1, message)
    {
    }
}
