﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Models.Util.Exceptions
{
    public class NotUniqueException : ClassroomException
    {
        public NotUniqueException(string className, string field, string? email)
            : base(2, $"Already exist a {className} with {field} field equals to: {email}")
        {
        }
    }
}
