﻿namespace Models;

public class Country
{
    public int? Id { get; }
    public string? Name { get; }
}
