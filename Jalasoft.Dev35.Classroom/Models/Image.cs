namespace Models;

public class Image
{
    public static readonly List<string> PermittedExtensions = new List<string> { ".png", ".jpg", ".jpeg" };

    public Image(FileUpload file, string baseUrl, string basePath, string folder, string name, long fileSizeLimit)
    {
        this.File = file;
        this.BaseUrl = baseUrl;
        this.BasePath = basePath;
        this.Folder = folder;
        this.Name = name;
        this.FileSizeLimit = fileSizeLimit;
    }

    public FileUpload File { get; init; }

    public string BaseUrl { get; init; }

    public string BasePath { get; init; }

    public string Folder { get; init; }

    public string Name { get; init; }

    public long FileSizeLimit { get; init; }

    public string Extension => Path.GetExtension(this.File.Files!.FileName).ToLowerInvariant();
}