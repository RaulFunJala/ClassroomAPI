﻿namespace Models;

public interface IFileWrapper
{
    bool Exists(string path);
}